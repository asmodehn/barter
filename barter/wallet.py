"""Model for a wallet"""
import typing
from dataclasses import dataclass
from decimal import Decimal

import dpcontracts

from barter.currency import Currency
from barter.price import Price

from barter.basket import Basket


@dataclass
class Wallet(Basket):

    """
    A basket for currencies
    """
    assets: typing.Dict[str, CurrencyAmount]

    @dpcontracts.require("`price` must be a Price", lambda args: isinstance(args.price, Price))
    @dpcontracts.require("`price.currency` must be the same as the Wallet's currency", lambda args: args.self.currency == args.price.currency)
    def __sub__(self, price: Price):
        with self.currency.context() as ctx:
            return Wallet(amount=self.amount - price.total_price, currency=self.currency)



