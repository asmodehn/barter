import typing


class Market:  # One market instance per asset

    asset: Asset
    prices: typing.Dict[Currency, UnitPrice]

    # TODO: mapping interface