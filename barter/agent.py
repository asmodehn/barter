import typing
from dataclasses import dataclass

from barter.currency import Currency
from barter.price import Price

from barter.asset import Asset
from barter.basket import Basket
from barter.wallet import Wallet


@dataclass
class Agent:
    wallets: typing.Dict[Currency, Wallet]
    baskets: typing.Dict[Asset, Basket]


    def __call__(self, price: Price):
        pass
