from dataclasses import dataclass

from barter.assetvolume import AssetVolume
from barter.unitprice import UnitPrice


@dataclass
class TotalPrice:
    asset: AssetVolume
    price: CurrencyAmount

    def context(self):
        return self.unit_price.currency.context

    def __call__(self, *args, **kwargs):
        """paying the price"""




# TODO : modify unitprice to add __mul__ (like we did for assetamount)
