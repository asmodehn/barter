from hypothesis import strategies as st

from barter import Asset

"""
A strategy to generate assets
"""

@st.composite
def asset_st(draw):
    uid = draw(st.characters(whitelist_categories=('Lu',)))
    return Asset(uid=uid)


if __name__ == '__main__':
    for n in range(1, 10):
        print(repr(asset_st().example()))




