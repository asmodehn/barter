from hypothesis import strategies as st

from barter import AssetQuantity

from barter.tests.strategies.st_asset import asset_st

"""
A strategy to generate asset quantities
"""

@st.composite
def asset_quantity_st(draw):
    quantity = draw(st.decimals(allow_nan=False, allow_infinity=False))
    return AssetQuantity(quantity=str(quantity), asset=draw(asset_st()))


if __name__ == '__main__':
    for n in range(1, 10):
        print(repr(asset_quantity_st().example()))

