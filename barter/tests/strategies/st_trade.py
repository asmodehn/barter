from hypothesis import strategies as st

from barter import AssetQuantity

from barter.tests.strategies.st_asset import asset_st
from barter.tests.strategies.st_assetquantity import asset_quantity_st
from barter.trade import Trade

"""
A strategy to generate asset quantities
"""

@st.composite
def trade_st(draw):
    return Trade(debit=draw(asset_quantity_st()), credit=draw(asset_quantity_st()))


if __name__ == '__main__':
    for n in range(1, 10):
        print(repr(trade_st().example()))

