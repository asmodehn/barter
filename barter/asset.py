"""
Model a currency
"""
from __future__ import annotations

import decimal
from dataclasses import dataclass, field

import dpcontracts
import typing
import weakref

import pint

from barter.assetcontext import AssetContext

from barter._registry import ureg


@dataclass(frozen=True, init=False)
class Asset:
    """
    Represent an Asset (a pint 'unit' with an associated precision)
    >>> U = Asset('U')
    >>> U.unit
    <Quantity(1, 'U')>
    >>> U.context
    Context(prec=28, rounding=ROUND_HALF_EVEN, Emin=-999999, Emax=999999, capitals=1, clamp=0, flags=[], traps=[InvalidOperation, DivisionByZero, Overflow])
    >>> U
    <Asset('U')>
    >>> print(U)
    U
    """

    unit: ureg.Quantity
    # TMP : while decimal in pint have issues... here we need custom precision depending on the unit.
    context: AssetContext = field(default_factory=AssetContext)

    def __init__(self, uid: str, definition: str = None, symbol=None, aliases: typing.List[str] = None, context: AssetContext = None):
        """
        Dynamically defining an asset / unit for pint
        :param uid:
        :param definition:
        :param aliases:
        :param context:
        """

        definition = definition if definition is not None else f'[]'  # dimensionless by default
        if aliases:
            symbol = symbol if symbol else '_'
            defstring = f"{uid} = {definition} = {symbol} = {' = '.join(aliases)}"  # TODO : test all possibilities...
        else:
            defstring = f"{uid} = {definition}" + (f" = {symbol}" if symbol else "")  # TODO : test all possibilities...

        object.__setattr__(self, 'context', context if context is not None else AssetContext())
        ureg.define(defstring)
        object.__setattr__(self, 'unit', ureg(uid))

    def __repr__(self):
        """ following same repr strategy as pint. but this represent an asset without magnitude"""
        return f"<Asset('{self.unit.units}')>"

    def __str__(self):
        """ following same repr strategy as pint """
        return f"{self.unit.units}"

    # __eq__ defined by dataclass

    # __hash__ relying on uid string
    def __hash__(self):
        """ we rely on the string definition of the unit for identity here """
        return hash(str(self.unit))

    def __call__(self, volume: str):
        """__call__ is defined dynamically by assetquantity. this is just here to help editors..."""
        raise NotImplementedError


if __name__ == '__main__':

    import doctest
    doctest.testmod()

