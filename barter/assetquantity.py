from __future__ import annotations

import decimal
import types
import typing
from dataclasses import dataclass, field

import dpcontracts

from barter._registry import ureg
from barter.asset import Asset
from barter.assetcontext import AssetContext


@dataclass(init=False, frozen=True)
class AssetQuantity:  # extends pint quantity by fixing pint's decimal shortcomings
    """
    An Asset Quantity, providing custom precision for computation

    >>> A = Asset('A')
    >>> a_one = A('0.1')
    >>> a_two = A('0.2')
    >>> a_three = a_one + a_two
    >>> a_three
    <AssetQuantity(0.3, A)>
    >>> print(a_three)
    0.3 A
    >>> a_four = a_three - A('0.05')
    >>> print(a_four)
    0.25 A
    """
    quantity: ureg.Quantity
    # TMP : while decimal in pint have issues... here we need custom precision depending on the unit...
    asset: Asset

    @property
    def context(self):
        return self.asset.context

    @dpcontracts.require("quantity must be a string or a quantity with the same unit", lambda args: isinstance(args.quantity, str) or (isinstance(args.quantity, ureg.Quantity) and args.quantity.units == args.asset.unit.units and isinstance(args.quantity.magnitude, decimal.Decimal)))
    def __init__(self, quantity: typing.Union[str, ureg.Quantity], asset: Asset):
        object.__setattr__(self, 'asset', asset)
        if isinstance(quantity, str):
            object.__setattr__(self, 'quantity', asset.context.create_decimal(quantity) * asset.unit)
        else:  # assume quantity with proper unit
            object.__setattr__(self, 'quantity', quantity)

    def __repr__(self):
        """ following same repr strategy as pint """
        return f"<AssetQuantity({self.quantity.magnitude}, {self.quantity.units})>"

    def __str__(self):
        """ following same repr strategy as pint """
        return f"{self.quantity.magnitude} {self.quantity.units}"


    # @dpcontracts.require("`uprice` must be a UnitPrice", lambda args: isinstance(args.uprice, UnitPrice))
    # @dpcontracts.require("`uprice` currency must not be the asset", lambda args: args.self.asset != args.uprice.currency)
    # def __mul__(self, uprice: UnitPrice):
    #     # should we be in price context or in asset context ???
    #     with decimal.localcontext(uprice.currency.context) as ctx:
    #         return TotalPrice(asset_volume=self, unit_price=uprice)

    @dpcontracts.require("`other` must be an AssetQuantity", lambda args: isinstance(args.other, AssetQuantity))
    @dpcontracts.require("`other` asset must be the same with self", lambda args: args.other.asset == args.self.asset)
    def __add__(self, other: AssetQuantity):
        with decimal.localcontext(self.context) as ctx:
            # TODO : avoid conversion if possible (but merge the context instances...)
            # CAREFUL with precision loss here
            return AssetQuantity(quantity=self.quantity + other.quantity, asset=self.asset)

    @dpcontracts.require("`other` must be an AssetQuantity", lambda args: isinstance(args.other, AssetQuantity))
    @dpcontracts.require("`other` asset must be the same with self", lambda args: args.other.asset == args.self.asset)
    def __sub__(self, other: AssetQuantity):
        with decimal.localcontext(self.context) as ctx:
            # TODO : avoid conversion if possible (but merge the context instances...)
            # CAREFUL with precision loss here
            return AssetQuantity(quantity=(self.quantity - other.quantity), asset=self.asset)


# Extending Asset method now that we know AssetQuantity:


@dpcontracts.require("`volume` must be a string to prevent accidental precision loss", lambda args: isinstance(args.volume, str))
@dpcontracts.ensure("returns an asset volume", lambda args, result: isinstance(result, AssetQuantity))
def asset_call(self, volume: str) -> AssetQuantity:

    return AssetQuantity(quantity=volume, asset=self)


Asset.__call__ = asset_call


@dpcontracts.require("`volume` must be a string to prevent accidental precision loss", lambda args: isinstance(args.volume, str))
@dpcontracts.ensure("returns an asset volume", lambda args, result: isinstance(result, AssetQuantity))
def asset_mul(self, volume: str) -> AssetQuantity:

    return self(volume)  # relying on __call__ implementation...


Asset.__mul__ = asset_mul


if __name__ == '__main__':

    import doctest
    doctest.testmod()

