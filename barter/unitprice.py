"""
Model of a Price
"""
import decimal
from dataclasses import dataclass

from barter.currencyasset import Currency

from barter.currencyamount import CurrencyAmount


@dataclass(init=False, frozen=True)
class UnitPrice:

    asset: Asset
    price: CurrencyAmount

    def __init__(self, asset: Asset, price: CurrencyAmount):
        super(UnitPrice, self).__init__(asset=currency, volume=amount)

    # Note : __add__ and __sub__ makes no sense here...





    # @dpcontracts.require("`avolume` must be an AssetVolume", lambda args: isinstance(args.avolume, AssetVolume))
    # @dpcontracts.require("`avolume.asset` must be different from currency", lambda args: args.avolume.asset != args.self.currency)
    # def __mul__(self, avolume: AssetVolume):
    #
    #     with decimal.localcontext(self.asset.context) as ctx:
    #         return TotalPrice(asset_volume=avolume, unit_price=self)


if __name__ == '__main__':

    pass

