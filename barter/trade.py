"""
Model a trade : teh event of currency/asset transitioning between agents.
"""
from datetime import datetime
from dataclasses import dataclass, field

from barter import AssetQuantity, Asset


@dataclass(frozen=True)
class Trade:
    """
    >>> import doctest
    >>> doctest.ELLIPSIS_MARKER = '-ignore-'
    >>> A = Asset('A')
    >>> B = Asset('B')
    >>> t = Trade(debit= A('2'), credit= B('-3'))
    >>> t  # doctest: +ELLIPSIS
    <Trade(quote=2 A, base=-3 B, timestamp=-ignore-)>
    >>> print(t)  #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    -ignore- 2 A -3 B

    A Trade is an event to track changes in time (from orders that have been successful).
    This is very similar to double entry accounting.
    """
    debit: AssetQuantity
    credit: AssetQuantity
    timestamp: datetime = field(default_factory= lambda: datetime.now())  # TODO : careful with timestamp /vs/ id

    def __call__(self, *args, **kwargs):
        # return the complement trade
        complement = Trade(debit = self.credit, credit= self.debit, timestamp = self.timestamp)
        return complement

    def __repr__(self):
        return f"<Trade(debit={self.debit}, credit={self.credit}, timestamp={self.timestamp})>"

    def __str__(self):
        """ csv/table like representation TODO : make it look like double entry accounting"""
        return f"{self.timestamp}\t{self.debit}\t{self.credit}"


    # TODO : find a way to aggregate these to determine PnL...


if __name__ == '__main__':
    import doctest
    doctest.testmod()
