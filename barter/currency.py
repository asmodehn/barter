from __future__ import annotations

import decimal
from dataclasses import dataclass, field

import dpcontracts

from barter.asset import Asset
from barter.assetcontext import AssetContext
from barter.assetvolume import AssetVolume


@dataclass(frozen=True)
class Currency:
    """ Just an Asset that can be used as price unit (after explicit 'conversion')
        Note : A volume can also be expressed with a (quote) currency as unit ...
     """

    uid: str
    context: AssetContext = field(default_factory=AssetContext)

    def __init__(self, asset: Asset, uid=None):
        object.__setattr__(self, 'context', asset.context)
        object.__setattr__(self, 'uid', uid if uid is not None else asset.uid)

    # __eq__ defined by dataclass

    @dpcontracts.require("`volume` must be a string to prevent accidental precision loss", lambda args: isinstance(args.amount, str))
    @dpcontracts.ensure("result is a decimal", lambda args, result: isinstance(result, decimal.Decimal))
    def __call__(self, amount: str = '0') -> decimal.Decimal:
        return self.context.create_decimal(amount)

    @dpcontracts.require("`other` must be an Asset", lambda args: isinstance(args.other, Currency))
    @dpcontracts.require("`other` must be the same Asset as self", lambda args: args.other.uid == args.self.uid)
    def __add__(self, other: Currency):
        return self

    @dpcontracts.require("`other` must be an Asset", lambda args: isinstance(args.other, Currency))
    @dpcontracts.require("`other` must be the same Asset as self", lambda args: args.other.uid == args.self.uid)
    def __sub__(self, other: Currency):
        return self
