import typing
from decimal import Decimal

import dpcontracts
from dataclasses import dataclass

from barter.asset import Asset
from barter.assetvolume import AssetVolume


@dataclass
class Basket:
    """
    A basket for assets
    """
    assets: typing.Dict[str, AssetVolume]  # str or Asset ??

    @dpcontracts.require("`price` must be a Price", lambda args: isinstance(args.price, Price))
    @dpcontracts.require("`price.currency` must be the same as the Wallet's currency",
                         lambda args: args.self.currency == args.price.currency)
    def __sub__(self, price: Price):
        with self.currency.context() as ctx:
            return Basket(amount=self.amount - price.total_price, currency=self.currency)
