from __future__ import annotations

import decimal
from dataclasses import dataclass

import dpcontracts

from barter.asset import Asset
from barter.currency import Currency


@dataclass(frozen=True)
class CurrencyAmount:  # TODO : since we redefine everything, maybe we dont need inheritance here ?? ducktyping vs type hints verification ?

    amount: decimal.Decimal
    currency: Currency

    def __init__(self, amount: str, currency: Currency):
        object.__setattr__(self, 'asset', amount)
        object.__setattr__(self, 'volume', self.currency(amount))

    @dpcontracts.require("`other` must be an AssetVolume", lambda args: isinstance(args.other, Asset))
    @dpcontracts.require("`other` asset must be the same Asset as self", lambda args: args.other.asset == args.self.asset)
    def __add__(self, other: CurrencyAmount):
        with decimal.localcontext(self.currency.context) as ctx:
            return CurrencyAmount(amount= self.amount + other.amount, currency=self.currency)

    @dpcontracts.require("`other` must be an Asset", lambda args: isinstance(args.other, Asset))
    @dpcontracts.require("`other` must be the same Asset as self", lambda args: args.other.uid == args.self.uid)
    def __sub__(self, other: CurrencyAmount):
        with decimal.localcontext(self.currency.context) as ctx:
            return CurrencyAmount(amount= self.amount - other.amount, currency=self.currency)
